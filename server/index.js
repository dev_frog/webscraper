const cheerio = require('cheerio')
const fetch = require('node-fetch')


const getNews = async(year,month,day) => {
    const url = `https://www.jugantor.com/archive/${year}/${month}/${day}`
    const response = await fetch(url);
    const body = await response.text()

    const $ = cheerio.load(body)
    const news_categories = []
    $('.archive-cat-caption').each((i, item) =>{
        const $item = $(item)
        const categories = $item.find('h6.ml-4').text();
        const news = {
            categories
        };
        news_categories.push(news)
    })
    console.log(news_categories)
};

getNews('2019','1','2')